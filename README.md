# Tarea 2. Desarrollo web avanzado

## Información general

* **Fecha:** 08/05/2021
* **Alumno:** Héctor Hugo Sandoval Marcelo
* **Asignatura:** Computación en el servidor Web
* **Repositorio:** [https://gitlab.com/hhsm95-maestria/csw-tarea-2](https://gitlab.com/hhsm95-maestria/csw-tarea-2)
* **Ver en línea:** [https://maestria-hhsm95.000webhostapp.com/semestre-1/computacion-servidor-web/tarea-2/](https://maestria-hhsm95.000webhostapp.com/semestre-1/computacion-servidor-web/tarea-2)

## Objetivo

En esta actividad se propone desarrollar una página web en PHP de temática libre. El
desarrollo se hará poniendo en práctica los conceptos estudiados en los dos primeros
temas. Por tanto, los requisitos mínimos serían los siguientes:

* Una estructura de control de cada tipo que hemos estudiado.
* Una función.
* Un array o matriz.
* Uso de alguna función de cadenas.

## Resultados

Se adjunta el código desarrollado y captura de pantalla del funcionamiento.

![Captura de pantalla](./assets/1.PNG "Captura de pantalla")

### El código index.php

```html
<?php

// Importa al menos una vez el archivo helpers.php
include_once('./helpers.php');

// Guarda la variable 'ip' si existe de otro modo guarda un string vacio.
$incomingIp = isset($_GET['ip']) ? $_GET['ip'] : '';
// Valida si hay una IP y si es valida.
if ($incomingIp != '' && ip2long($incomingIp) !== false) {
    $ip = $incomingIp;
    $validIp = true;
} else {
    $ip = getRealIp();
    $validIp = false;
}
// Crea una variable para determinar si debe mostrar un error o no.
$showError = $validIp == false && $incomingIp != '';

// Obtiene la plataforma del sistema actual.
$plataform = getPlatform();
// Obtiene el nombre del navegador del sistema actual.
$browser = getBrowser();
// Obtiene información de la IP.
$ipInfo = callIpInfoApi($ip);

?>

<!doctype html>
<html lang="es-MX">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

    <style>
        body {
            color: var(--bs-light);
        }

        .u-bg-dark {
            background-color: var(--bs-gray-dark);
        }

        .u-bg-darker {
            background-color: var(--bs-dark);
        }

        #map {
            height: 400px;
        }
    </style>
    <title>Tarea 2 - Computación en el servidor Web</title>
</head>

<body class="u-bg-dark">

    <main class="container">
        <div class="row mt-2 mb-4">
            <div clas="col">
                <h3 class="text-center u-bg-darker rounded p-2">Tarea 2 - Computación en el servidor Web</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-4 mb-2">
                <div class="u-bg-darker rounded p-2">
                    <h5>Tu información</h5>
                    <ul>
                        <li>Plataforma: <code><?php echo ($plataform) ?></code></li>
                        <li>Navegador: <code><?php echo ($browser) ?></code></li>
                    </ul>
                </div>
            </div>

            <div class="col-xs-12 col-md-6 col-lg-4 mb-2">
                <div class="u-bg-darker rounded p-2">
                    <h5>Consulta información de una IP</h5>
                    <div class="mb-3">
                        <label for="txtIp" class="form-label">IP:</label>
                        <input type="text" onkeypress="clearWarning()" class="form-control <?php if ($showError) { ?> is-invalid <?php } ?>" id="txtIp" placeholder="<?php echo ($ip) ?>" value="<?php echo ($ip) ?>">
                        <!-- Si hay error mostrar el mensaje de aviso que la IP es inválida. -->
                        <?php if ($showError) { ?>
                            <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                La IP (<?php echo ($incomingIp) ?>) introducida no es válida
                            </div>
                        <?php } ?>
                    </div>
                    <div class="text-center">
                        <button id="btnSearch" type="button" class="btn btn-primary" onclick="buscarIp();">Buscar</button>
                    </div>
                    <p>Resultados:</p>
                    <ul>
                        <!-- Utiliza foreach para recorrer a traves de la información de la IP, por clave y valor. -->
                        <?php foreach ($ipInfo as $clave => $valor) { ?>
                             <!-- Utiliza switch para diferenciar el tipo de clave que viene en la información de la IP -->
                            <?php switch ($clave) {
                                case "location": // Cuando es location:
                            ?>
                                    <li><?php echo ("geoname_id: <code>{$valor->geoname_id}</code>") ?></li>
                                    <li><?php echo ("capital: <code>{$valor->capital}</code>") ?></li>
                                    <?php $num_langs = count($valor->languages);
                                    $j = 0;
                                    // Utiliza while para recorrer el número de lenguajes que puedan venir en la información de la IP
                                    while ($j < $num_langs) { ?>
                                        <li><?php echo ("languages: <code>{$valor->languages[$j]->name}</code>") ?></li>
                                    <?php $j += 1;
                                    }
                                    break;
                                default: // En cualquier otro caso: ?>
                                    <li><?php echo ("{$clave}: <code>{$valor}</code>") ?></li>
                        <?php }
                        } ?>

                    </ul>
                </div>
            </div>

            <!-- Muestra una mapa con la geolocalización de la IP cuando no hay error -->
            <?php if (!$showError) { ?>
                <div class="col-xs-12 col-lg-4">
                    <div id="map">
                    </div>
                </div>
            <?php } ?>
        </div>

    </main>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>

    <script>
        function buscarIp() {
            const ip = document.getElementById("txtIp").value;
            const pathname = window.location.pathname;
            const button = document.getElementById("btnSearch");
            button.innerHTML = `<div class="spinner-border text-light" role="status"></div>`;
            setTimeout(function() {
                window.location.replace(`${pathname}?ip=${ip}`);
            }, 250);
        }

        function clearWarning() {
            const input = document.getElementById("txtIp");
            input.classList.remove("is-invalid");
        }

        // Muestra una mapa con la geolocalización de la IP cuando no hay error
        <?php if (!$showError) { ?>

            const lat = "<?php echo ($ipInfo->latitude) ?>";
            const long = "<?php echo ($ipInfo->longitude) ?>";

            const map = L.map('map').setView([lat, long], 11);

            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);
            L.marker([lat, long]).addTo(map)
                .bindPopup('Puedo verte 👀')
                .openPopup();

        <?php } ?>
    </script>

</body>

</html>
```

### Código helpers.php

```php
<?php
// Define una constante para almacenar clave API que consumirá la obtención de información de la IP.
define("API_KEY", "93aa2ad12af49c71177de705c51eaa69");

// Función para obtener la IP real del cliente.
function getRealIp()
{
    // Verificar a traves de múltiples headers en los que probablemente podría venir la IP del cliente.
    if (isset($_SERVER["HTTP_CLIENT_IP"])) {
        return $_SERVER["HTTP_CLIENT_IP"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED"])) {
        return $_SERVER["HTTP_X_FORWARDED"];
    } elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_FORWARDED"])) {
        return $_SERVER["HTTP_FORWARDED"];
    } else {
        return $_SERVER["REMOTE_ADDR"];
    }
}

// Función que obtiene el nombre de la plataforma que visita el sitio.
function getPlatform()
{
    // Toma el user agent del header del request.
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    // Creamos un arreglo asociativo para almacenar las posibles plataformas.
    $plataformas = array(
        'Windows 10' => 'Windows NT 10.0+',
        'Windows 8.1' => 'Windows NT 6.3+',
        'Windows 8' => 'Windows NT 6.2+',
        'Windows 7' => 'Windows NT 6.1+',
        'Windows Vista' => 'Windows NT 6.0+',
        'Windows XP' => 'Windows NT 5.1+',
        'Windows 2003' => 'Windows NT 5.2+',
        'Windows' => 'Windows otros',
        'iPhone' => 'iPhone',
        'iPad' => 'iPad',
        'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
        'Mac otros' => 'Macintosh',
        'Android' => 'Android',
        'BlackBerry' => 'BlackBerry',
        'Linux' => 'Linux',
    );
    // Recorremos con un foreach nuestro arreglo de plataformas y buscamos con regex si encontramos el patrón de alguna de ellas dentro del user agent y retornamos el valor.
    foreach ($plataformas as $plataforma => $pattern) {
        if (preg_match('/' . $pattern . '/', $user_agent)) {
            return $plataforma;
        }
    }
    // De otro modo retornamos un valor default como 'Otras'.
    return 'Otras';
}

// Función para obtener el navegador que esta utilizando el cliente.
function getBrowser()
{
    // Nuevamente tomamos el user agent del header del request.
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    // Pero esta vez buscamos con ifs el patrón del user agent por el valor que relaciona a algunos de los navegadores mas populares.
    if (strpos($user_agent, 'MSIE') !== false) {
        return 'Internet explorer';
    } elseif (strpos($user_agent, 'Edge') !== false) {
        return 'Microsoft Edge';
    } elseif (strpos($user_agent, 'Trident') !== false) {
        return 'Internet explorer';
    } elseif (strpos($user_agent, 'Opera Mini') !== false) {
        return "Opera Mini";
    } elseif (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR') !== false) {
        return "Opera";
    } elseif (strpos($user_agent, 'Firefox') !== false) {
        return 'Mozilla Firefox';
    } elseif (strpos($user_agent, 'Chrome') !== false) {
        return 'Google Chrome';
    } elseif (strpos($user_agent, 'Safari') !== false) {
        return "Safari";
    } else {
        return 'No hemos podido detectar tu navegador';
    }
}

// Función que llama a una API externa y obtiene la información de la IP solicitida.
function callIpInfoApi($ip)
{
    // Construye la URL a consultar, se incluye la IP y el API_KEY.
    $url = "http://api.ipapi.com/" . $ip . "?access_key=" . API_KEY . "&format=1";
    // Se consulta la API y se almacena el resultado.
    $loc = file_get_contents($url);
    // Se convierte el resultado JSON en un objecto de PHP y se devuelve.
    return json_decode($loc);
}
```
