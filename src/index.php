<?php

include_once('./helpers.php');

$incomingIp = isset($_GET['ip']) ? $_GET['ip'] : '';
if ($incomingIp != '' && ip2long($incomingIp) !== false) {
    $ip = $incomingIp;
    $validIp = true;
} else {
    $ip = getRealIp();
    $validIp = false;
}
$showError = $validIp == false && $incomingIp != '';

$plataform = getPlatform();
$browser = getBrowser();
$ipInfo = callIpInfoApi($ip);

?>

<!doctype html>
<html lang="es-MX">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

    <style>
        body {
            color: var(--bs-light);
        }

        .u-bg-dark {
            background-color: var(--bs-gray-dark);
        }

        .u-bg-darker {
            background-color: var(--bs-dark);
        }

        #map {
            height: 400px;
        }
    </style>
    <title>Tarea 2 - Computación en el servidor Web</title>
</head>

<body class="u-bg-dark">

    <main class="container">
        <div class="row mt-2 mb-4">
            <div clas="col">
                <h3 class="text-center u-bg-darker rounded p-2">Tarea 2 - Computación en el servidor Web</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-4 mb-2">
                <div class="u-bg-darker rounded p-2">
                    <h5>Tu información</h5>
                    <ul>
                        <li>Plataforma: <code><?php echo ($plataform) ?></code></li>
                        <li>Navegador: <code><?php echo ($browser) ?></code></li>
                    </ul>
                </div>
            </div>

            <div class="col-xs-12 col-md-6 col-lg-4 mb-2">
                <div class="u-bg-darker rounded p-2">
                    <h5>Consulta información de una IP</h5>
                    <div class="mb-3">
                        <label for="txtIp" class="form-label">IP:</label>
                        <input type="text" onkeypress="clearWarning()" class="form-control <?php if ($showError) { ?> is-invalid <?php } ?>" id="txtIp" placeholder="<?php echo ($ip) ?>" value="<?php echo ($ip) ?>">
                        <?php if ($showError) { ?>
                            <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                La IP (<?php echo ($incomingIp) ?>) introducida no es válida
                            </div>
                        <?php } ?>
                    </div>
                    <div class="text-center">
                        <button id="btnSearch" type="button" class="btn btn-primary" onclick="buscarIp();">Buscar</button>
                    </div>
                    <p>Resultados:</p>
                    <ul>
                        <?php foreach ($ipInfo as $clave => $valor) { ?>
                            <?php switch ($clave) {
                                case "location":
                            ?>
                                    <li><?php echo ("geoname_id: <code>{$valor->geoname_id}</code>") ?></li>
                                    <li><?php echo ("capital: <code>{$valor->capital}</code>") ?></li>
                                    <?php $num_langs = count($valor->languages);
                                    $j = 0;
                                    while ($j < $num_langs) { ?>
                                        <li><?php echo ("languages: <code>{$valor->languages[$j]->name}</code>") ?></li>
                                    <?php $j += 1;
                                    }
                                    break;
                                default: ?>
                                    <li><?php echo ("{$clave}: <code>{$valor}</code>") ?></li>
                        <?php }
                        } ?>

                    </ul>
                </div>
            </div>

            <?php if (!$showError) { ?>
                <div class="col-xs-12 col-lg-4">
                    <div id="map">
                    </div>
                </div>
            <?php } ?>
        </div>

    </main>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>

    <script>
        function buscarIp() {
            const ip = document.getElementById("txtIp").value;
            const pathname = window.location.pathname;
            const button = document.getElementById("btnSearch");
            button.innerHTML = `<div class="spinner-border text-light" role="status"></div>`;
            setTimeout(function() {
                window.location.replace(`${pathname}?ip=${ip}`);
            }, 250);
        }

        function clearWarning() {
            const input = document.getElementById("txtIp");
            input.classList.remove("is-invalid");
        }

        <?php if (!$showError) { ?>

            const lat = "<?php echo ($ipInfo->latitude) ?>";
            const long = "<?php echo ($ipInfo->longitude) ?>";

            const map = L.map('map').setView([lat, long], 11);

            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);
            L.marker([lat, long]).addTo(map)
                .bindPopup('Puedo verte 👀')
                .openPopup();

        <?php } ?>
    </script>

</body>

</html>