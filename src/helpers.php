<?php

define("API_KEY", "93aa2ad12af49c71177de705c51eaa69");

function getRealIp()
{
    if (isset($_SERVER["HTTP_CLIENT_IP"])) {
        return $_SERVER["HTTP_CLIENT_IP"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED"])) {
        return $_SERVER["HTTP_X_FORWARDED"];
    } elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_FORWARDED"])) {
        return $_SERVER["HTTP_FORWARDED"];
    } else {
        return $_SERVER["REMOTE_ADDR"];
    }
}

function getPlatform()
{
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $plataformas = array(
        'Windows 10' => 'Windows NT 10.0+',
        'Windows 8.1' => 'Windows NT 6.3+',
        'Windows 8' => 'Windows NT 6.2+',
        'Windows 7' => 'Windows NT 6.1+',
        'Windows Vista' => 'Windows NT 6.0+',
        'Windows XP' => 'Windows NT 5.1+',
        'Windows 2003' => 'Windows NT 5.2+',
        'Windows' => 'Windows otros',
        'iPhone' => 'iPhone',
        'iPad' => 'iPad',
        'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
        'Mac otros' => 'Macintosh',
        'Android' => 'Android',
        'BlackBerry' => 'BlackBerry',
        'Linux' => 'Linux',
    );
    foreach ($plataformas as $plataforma => $pattern) {
        if (preg_match('/' . $pattern . '/', $user_agent)) {
            return $plataforma;
        }
    }
    return 'Otras';
}

function getBrowser()
{
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    if (strpos($user_agent, 'MSIE') !== false) {
        return 'Internet explorer';
    } elseif (strpos($user_agent, 'Edge') !== false) { //Microsoft Edge
        return 'Microsoft Edge';
    } elseif (strpos($user_agent, 'Trident') !== false) { //IE 11
        return 'Internet explorer';
    } elseif (strpos($user_agent, 'Opera Mini') !== false) {
        return "Opera Mini";
    } elseif (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR') !== false) {
        return "Opera";
    } elseif (strpos($user_agent, 'Firefox') !== false) {
        return 'Mozilla Firefox';
    } elseif (strpos($user_agent, 'Chrome') !== false) {
        return 'Google Chrome';
    } elseif (strpos($user_agent, 'Safari') !== false) {
        return "Safari";
    } else {
        return 'No hemos podido detectar tu navegador';
    }
}

function callIpInfoApi($ip)
{
    $url = "http://api.ipapi.com/" . $ip . "?access_key=" . API_KEY . "&format=1";
    $loc = file_get_contents($url);
    return json_decode($loc);
}
